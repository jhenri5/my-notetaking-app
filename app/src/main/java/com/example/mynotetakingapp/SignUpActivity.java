package com.example.mynotetakingapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SignUpActivity extends AppCompatActivity {

    EditText email_et, password_et, password_reenter_et, name_et;
    Button signup_btn;
    ProgressBar progressBar;

    private String email, password, reentered_password, name;
    ValidateInput validateInput;

    FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        // [START] Sign-up Part

        email_et = findViewById(R.id.email_et);
        password_et = findViewById(R.id.password_et);
        password_reenter_et = findViewById(R.id.password_reenter_et);
        name_et = findViewById(R.id.name_et);

        signup_btn = findViewById(R.id.login_btn);

        validateInput = new ValidateInput(this); //Needed in order to validate the email and the passwords

        signup_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Sign Up the User
                handleSignUpClick();
            }
        });

        progressBar = findViewById(R.id.progressBar);

        auth = FirebaseAuth.getInstance();
        // [END] Sign-up Part
    }

    private void handleSignUpClick() {

        // Fetching the string values
        email = email_et.getText().toString();
        password = password_et.getText().toString();
        reentered_password = password_reenter_et.getText().toString();
        name = name_et.getText().toString();

        if (name.isEmpty()) {
            hideProgressBar();
            Toast.makeText(this, "Enter your name", Toast.LENGTH_SHORT).show();
        } else {

            if (validateInput.emailValid(email) && validateInput.passwordValid(password)) {

                if (password.equals(reentered_password)) { // If re-entered password and password are the same

                    //signUp the user with this email and password
                    auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            // Triggered when the user is created using firebase auth
                            if (task.isSuccessful()) {
                                hideProgressBar();
                                FirebaseUser user = auth.getCurrentUser(); //Reps the user's profile info in my Firebase project's user database
                                Toast.makeText(SignUpActivity.this, "Sign-up is successful for: "+user.getEmail(), Toast.LENGTH_SHORT).show();

                                saveNameInFirebaseRealtimeDatabase(user);

                            } else {
                                hideProgressBar();
                                Toast.makeText(SignUpActivity.this, "Error occured: "+task.getException(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                } else {
                    hideProgressBar();
                    Toast.makeText(this, "Passwords don't match. Please re-enter the passwords", Toast.LENGTH_SHORT).show();
                }

            }

        }

    }

    private void saveNameInFirebaseRealtimeDatabase(FirebaseUser user) {

        //firebase method
        FirebaseDatabase fireDB = FirebaseDatabase.getInstance();
        DatabaseReference root = fireDB.getReference(); //Gets the database reference of the root node

        DatabaseReference name_ref = root.child("Users").child(user.getUid()).child("name");
        name_ref.setValue(name);
    }

    private void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar() {
        progressBar.setVisibility(View.INVISIBLE);
    }
}