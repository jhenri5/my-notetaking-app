package com.example.mynotetakingapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class UserAccountActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView welcome_tv, email_tv;
    private Button log_out_btn, updated_password_btn, updated_email_btn;

    FirebaseAuth auth;
    FirebaseUser user;

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_account);

        welcome_tv = findViewById(R.id.welcome_tv);
        email_tv = findViewById(R.id.email_tv);
        log_out_btn = findViewById(R.id.log_out_btn);
        updated_password_btn = findViewById(R.id.update_password_btn);
        updated_email_btn = findViewById(R.id.update_email_btn);

        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();

        email_tv.setText(user.getEmail());

        log_out_btn.setOnClickListener(this);
        updated_password_btn.setOnClickListener(this);
        updated_email_btn.setOnClickListener(this);

        context = this;

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.log_out_btn:
                showLogoutDialog();
                break;
            case R.id.update_password_btn:
                showUpdatePasswordActivity();
                break;
            case R.id.update_email_btn:
                showUpdateEmailAcitivty();
                break;
        }
    }

    private void showUpdateEmailAcitivty() {
        Toast.makeText(context, "Update Email Here!", Toast.LENGTH_SHORT).show();

        Intent updateEmailActivity = new Intent(UserAccountActivity.this, UpdateEmailActivity.class);
        startActivity(updateEmailActivity);
    }

    private void showUpdatePasswordActivity() {
        Toast.makeText(context, "Update Password Here!", Toast.LENGTH_SHORT).show();

        Intent updatePasswordActivty = new Intent(UserAccountActivity.this, UpdatePasswordActivity.class);
        startActivity(updatePasswordActivty);
    }

    private void showLogoutDialog() {

        AlertDialog.Builder builder = new  AlertDialog.Builder(context);
        builder.setMessage("Are yu sure you want to logout?").setPositiveButton("Yes, logout", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                auth.signOut(); // end the session
                ((Activity)context).finish(); //end current activity
            }
        }).setNegativeButton("No!", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}