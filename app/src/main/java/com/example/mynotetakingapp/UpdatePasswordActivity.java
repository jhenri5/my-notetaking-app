package com.example.mynotetakingapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class UpdatePasswordActivity extends AppCompatActivity {

    private EditText new_password_et, new_password_reenter_et;
    private Button update_password_btn;

    ValidateInput validateInput;
    FirebaseAuth auth;
    FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_password);

        new_password_et = findViewById(R.id.new_password_et);
        new_password_reenter_et = findViewById(R.id.new_password_reenter_et);
        update_password_btn = findViewById(R.id.update_password_btn);

        update_password_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateFirebaseInFirebase();
            }
        });

        validateInput = new ValidateInput(this);

        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
    }

    private void updateFirebaseInFirebase() {
        String new_password = new_password_et.getText().toString();
        String new_password_reenter = new_password_reenter_et.getText().toString();

        if (validateInput.passwordValid(new_password)) {

            if(new_password.equals(new_password_reenter)) {
                user.updatePassword(new_password_reenter).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        if (task.isSuccessful()) {
                            // password updated successfully
                            Toast.makeText(UpdatePasswordActivity.this, "Password is updated", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(UpdatePasswordActivity.this, "Error occured: "+task.getException(), Toast.LENGTH_SHORT).show();
                        }

                    }
                });
            }

        } else {
            Toast.makeText(this, "Passwords don't match!", Toast.LENGTH_SHORT).show();
        }
    }
}