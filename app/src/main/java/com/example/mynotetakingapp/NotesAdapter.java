package com.example.mynotetakingapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.ViewHolder> {

    private final ArrayList<Note> noteList;
    private final Context context;

    public NotesAdapter(ArrayList<Note> notesList, Context myContext) {
        this.noteList = notesList;
        this.context = myContext;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView note_title, note_content, edit_note, delete_note;

        public ViewHolder(View itemView) {
            super(itemView);

            note_content = itemView.findViewById(R.id.note_content_tv);
            note_title = itemView.findViewById(R.id.note_title_tv);
            edit_note = itemView.findViewById(R.id.edit_note_tv);
            delete_note = itemView.findViewById(R.id.delete_note_tv);
        }

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        //Create a view
        View noteView = LayoutInflater.from(parent.getContext()).inflate(R.layout.note_card, parent, false);

        ViewHolder viewHolder = new ViewHolder(noteView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        //We will set the data here
        final Note note = noteList.get(position);
        holder.note_title.setText(note.getNoteTitle());
        holder.note_content.setText(note.getNoteContent());

        //handle the event when you click on edit TextView
        holder.edit_note.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //We will open Edit note activity here.
                Intent editNoteIntent = new Intent(context, EditNoteActivity.class);

                editNoteIntent.putExtra("noteTitle", note.getNoteTitle());
                editNoteIntent.putExtra("noteContent", note.getNoteContent());
                editNoteIntent.putExtra("noteID", note.getNoteID());

                context.startActivity(editNoteIntent);
            }
        });

        holder.delete_note.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDeleteNoteDialog(note.getNoteID());
            }
        });

    }

    private void showDeleteNoteDialog(final String noteID) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Are you sure you want to delete this note?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        //Delete the note here
                        ((HomeActivity) context).deleteNoteFromFirebase(noteID); //HomeActivity.deleteNoteFromFirebase()
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        //User cancelled the Dialog
                        dialogInterface.dismiss();
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public int getItemCount() {
        return noteList.size();
    }


}
