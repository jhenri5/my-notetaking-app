package com.example.mynotetakingapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class CreateNoteActivity extends AppCompatActivity {

    EditText note_title, note_content;
    Button create_note_btn;

    CreateNoteActivity context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_note_ativity);

        note_title = findViewById(R.id.note_title_et);
        note_content = findViewById(R.id.note_content_et);
        create_note_btn = findViewById(R.id.create_note_btn);

        create_note_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                putNoteInFirebase();
            }
        });

        context = CreateNoteActivity.this;
    }

    private void putNoteInFirebase() {

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser(); //user is needed to get get current user ID

        FirebaseDatabase fireDB = FirebaseDatabase.getInstance();
        DatabaseReference root = fireDB.getReference(); //reference to database-root

        DatabaseReference notes_ref = root.child("Users").child(user.getUid()).child("Notes"); //root/Users/(current user ID)/Notes

        DatabaseReference new_notes_ref = notes_ref.push(); //root/Users/(currentUserID)/Notes/(notesID)

        // A Note ID : {note_title: "Title", note_content: "Content"} Note class will have these two properties, note_title and note_content
        Note new_note = new Note(note_title.getText().toString(), note_content.getText().toString());
        new_notes_ref.setValue(new_note).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                if (task.isSuccessful()) {
                    Toast.makeText(CreateNoteActivity.this, "The note is submitted to Firebase.", Toast.LENGTH_SHORT).show();
                    context.finish(); //finish this activity
                } else {
                    Toast.makeText(CreateNoteActivity.this, "Some error occurred: "+task.getException(), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}