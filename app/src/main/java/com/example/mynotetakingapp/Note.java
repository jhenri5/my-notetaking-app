package com.example.mynotetakingapp;

import com.google.firebase.database.Exclude;

public class Note {

    public String noteTitle, noteContent;

    @Exclude  //Excludes this variable from the database
    String noteID;


    public Note() {
        //this is required for Firebase
    }

    public Note(String noteTitle, String noteContent) {
        this.noteTitle = noteTitle;
        this.noteContent = noteContent;
    }

    @Exclude
    public String getNoteID() {
        return noteID;
    }

    @Exclude
    public void setNoteID(String noteID) {
        this.noteID = noteID;
    }

    public String getNoteTitle() {
        return noteTitle;
    }

    public void setNoteTitle(String noteTitle) {
        this.noteTitle = noteTitle;
    }

    public String getNoteContent() {
        return noteContent;
    }

    public void setNoteContent(String noteContent) {
        this.noteContent = noteContent;
    }
}
