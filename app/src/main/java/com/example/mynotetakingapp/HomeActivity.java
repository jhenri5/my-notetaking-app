package com.example.mynotetakingapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {

    TextView welcome_msg_tv;
    Button create_note_btn;
    RecyclerView recycler_view;

    ArrayList<Note> noteArrayList = new ArrayList<>();

    NotesAdapter notesAdapter;

    Context myContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        myContext = this;

        //get the name of the user
        welcome_msg_tv = findViewById(R.id.welcome_msg_tv);

        create_note_btn = findViewById(R.id.create_note_btn);

        //get the name from Firebase
        FirebaseDatabase fireDB  = FirebaseDatabase.getInstance();
        DatabaseReference root = fireDB.getReference(); //app root in FireBase Database

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        DatabaseReference name_ref = root.child("Users").child(user.getUid()).child("name");

        name_ref.addListenerForSingleValueEvent(new ValueEventListener() { //Will get triggered at least once
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                //snapshot will have = {name: "Jeff"}
                welcome_msg_tv.setText("Hi, welcome "+snapshot.getValue().toString()+"!");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        create_note_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCreateNodeActivity();
            }
        });

        // Setting up LinearLayoutManager
        recycler_view = findViewById(R.id.recycler_view);
        recycler_view.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recycler_view.setLayoutManager(linearLayoutManager);

        //create the adapter


        // Getting the Notes from Firebase
        readNotesFromFirebase();
    }

    private void readNotesFromFirebase() {

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        FirebaseDatabase fireDB = FirebaseDatabase.getInstance();
        DatabaseReference notes_ref = fireDB.getReference().child("Users").child(user.getUid()).child("Notes");

        // read the notes from Firebase
        notes_ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                noteArrayList.clear(); //Clear the list every time a change is made, so we don't see any duplicates

                //this snapshot will contain: {NoteID: {note_title: "Some title", note_content: "Some Content"}, NoteID2: {note_title: "Some title", note_content: "Some Content"}}
                Note note;
                for (DataSnapshot noteSnapShot : snapshot.getChildren()) {
                    note = noteSnapShot.getValue(Note.class);

                    note.setNoteID(noteSnapShot.getKey()); //Grabbing the note's id through noteSanpShot's get Key method

                    //Toast.makeText(HomeActivity.this, "note: title : "+note.getNoteTitle() + ", content "+note.getNoteContent(), Toast.LENGTH_SHORT).show();
                    Log.i("mynote", "note: title : "+note.getNoteTitle() + " content "+note.getNoteContent());

                    // addd note to the arraylist of Notes
                    noteArrayList.add(note);

                }

                // Set Up Layout
                notesAdapter = new NotesAdapter(noteArrayList, myContext);
                recycler_view.setAdapter(notesAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void openCreateNodeActivity() {

        //TOOO: Open CreateNoteActivity
        Intent createNoteActivity = new Intent(HomeActivity.this, CreateNoteActivity.class);
        startActivity(createNoteActivity);
    }

    public void deleteNoteFromFirebase(String noteID) {

        //Delete note in Firebase
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        FirebaseDatabase fireDB = FirebaseDatabase.getInstance();
        DatabaseReference notes_ref = fireDB.getReference().child("Users").child(user.getUid()).child("Notes");

        DatabaseReference particular_note = notes_ref.child(noteID);

        // delete method in Firebase

        particular_note.removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                if (task.isSuccessful()) {
                    Toast.makeText(myContext, "Note is successfully deleted", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(myContext, "Some error occurred: "+task.getException(), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.user_account:
                Intent userAccountIntent = new Intent(HomeActivity.this, UserAccountActivity.class);
                startActivity(userAccountIntent);
                break;
            case R.id.log_out:
                Intent logoutIntent = new Intent(HomeActivity.this, LoginActivity.class);
                startActivity(logoutIntent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}