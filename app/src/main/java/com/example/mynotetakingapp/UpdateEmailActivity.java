package com.example.mynotetakingapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class UpdateEmailActivity extends AppCompatActivity {

    EditText old_email_et, new_email_et;
    Button update_email_btn;

    ValidateInput validateInput;

    FirebaseAuth auth;
    FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_email);

        old_email_et = findViewById(R.id.old_email_et);
        new_email_et = findViewById(R.id.new_email_et);
        update_email_btn = findViewById(R.id.update_email_btn);

        update_email_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateEmailInFirebase();
            }
        });

        validateInput = new ValidateInput(this);

        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();

    }

    private void updateEmailInFirebase() {
        String old_email = old_email_et.getText().toString();
        String new_email = new_email_et.getText().toString();

        if (validateInput.emailValid(new_email)) {

            if (user.getEmail().matches(old_email)) { //Gotta make sure that the old email is exactly the same as the new email

                if (old_email.equals(new_email)) {
                    Toast.makeText(this, "Enter a new email", Toast.LENGTH_SHORT).show();
                } else {

                    //update email
                    user.updateEmail(new_email).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {

                            if (task.isSuccessful()) {
                                Toast.makeText(UpdateEmailActivity.this, "Email update successfully", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(UpdateEmailActivity.this, "Error occcured: "+task.getException(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }

            } else {
                Toast.makeText(this, "Make sure you entered your current email correctly", Toast.LENGTH_SHORT).show();
            }

        }
    }
}