package com.example.mynotetakingapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class EditNoteActivity extends AppCompatActivity {

    EditText edit_note_title_et, edit_note_content_et;
    Button edit_note_btn;

    EditNoteActivity context;

    String note_title, note_content, note_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_note);

        edit_note_title_et = findViewById(R.id.edit_note_title_et);
        edit_note_content_et = findViewById(R.id.edit_note_content_et);
        edit_note_btn = findViewById(R.id.edit_note_btn);

        if (getIntent().getExtras() != null) {
            note_title = getIntent().getStringExtra("noteTitle");
            note_content = getIntent().getStringExtra("noteContent");
            note_id = getIntent().getStringExtra("noteID");

            edit_note_title_et.setText(note_title);
            edit_note_content_et.setText(note_content);
        }

        edit_note_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editNoteInFirebase();
            }
        });

        context = EditNoteActivity.this;
    }

    private void editNoteInFirebase() {
        //edit the note whose ID is something
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        FirebaseDatabase fireDB = FirebaseDatabase.getInstance();
        DatabaseReference root = fireDB.getReference();
        DatabaseReference notes_ref = root.child("Users").child(user.getUid()).child("Notes");

        DatabaseReference particular_note_ref = notes_ref.child(note_id);
        particular_note_ref.child("noteTitle").setValue(edit_note_title_et.getText().toString());
        particular_note_ref.child("noteContent").setValue(edit_note_content_et.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                if (task.isSuccessful()) {

                    //note updated successfully
                    Toast.makeText(EditNoteActivity.this, "Note updated sucessfully", Toast.LENGTH_SHORT).show();
                    context.finish(); //finish this activity

                } else {
                    Toast.makeText(EditNoteActivity.this, "Some error occurred!", Toast.LENGTH_SHORT).show();

                }
            }
        });
    }
}