package com.example.mynotetakingapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    EditText email_et, password_et;
    TextView signup_tv;
    Button login_btn;
    String email, password;
    ProgressBar progressBar;

    ValidateInput validateInput;
    FirebaseAuth auth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // [START] Login Part

        validateInput = new ValidateInput(this);

        email_et = findViewById(R.id.email_et);
        password_et = findViewById(R.id.password_et);
        login_btn = findViewById(R.id.login_btn);
        signup_tv = findViewById(R.id.signup_tv);

        auth = FirebaseAuth.getInstance();

        // [END} Login Part

        login_btn.setOnClickListener(this);
        signup_tv.setOnClickListener(this);

        progressBar = findViewById(R.id.progressBar);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.login_btn:
                handleLoginClick();
                break;
            case R.id.signup_tv:
                handleSingUpClick();
                break;
        }
    }

    private void handleLoginClick() {

        showProgressBar();
        email = email_et.getText().toString();
        password = password_et.getText().toString();

        // Validate the email and password fields input
        if (validateInput.emailValid(email) && validateInput.passwordValid(password)) {
            Toast.makeText(this, "Valid Inputs", Toast.LENGTH_SHORT).show();

            auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful()) {
                        hideProgressBar();
                        Toast.makeText(LoginActivity.this, "Logged in user "+email, Toast.LENGTH_SHORT).show();

                        //Show another screen on login. See how to logout
                        Intent userAccountActivity = new Intent(LoginActivity.this, HomeActivity.class);
                        startActivity(userAccountActivity);

                    } else {
                        hideProgressBar();
                        Toast.makeText(LoginActivity.this, "Some error occurred: "+task.getException(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private void handleSingUpClick() {
        Toast.makeText(this, "Sign Up Here!", Toast.LENGTH_SHORT).show();

        Intent signUpActivity = new Intent(LoginActivity.this, SignUpActivity.class);
        startActivity(signUpActivity);
    }

    private void hideProgressBar() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    private void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }
}